<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as FormTypes;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Comment;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName', FormTypes\TextType::class, [
                'constraints' => [
                    new Constraints\NotBlank,
                ],
            ])
            ->add('userEmail', FormTypes\EmailType::class, [
                'constraints' => [
                    new Constraints\NotBlank,
                    new Constraints\Email,
                ],
            ])
            ->add('comment', FormTypes\TextType::class, [
                'constraints' => [
                    new Constraints\NotBlank,
                ],
            ])
            ->add('avatar', FormTypes\FileType::class, [
                'required'    => false,
                'constraints' => [
                    new Constraints\Image,
                ],
            ])
            ->add('send', FormTypes\SubmitType::class);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Comment::class,
        ));
    }
}
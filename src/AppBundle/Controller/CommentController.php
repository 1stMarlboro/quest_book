<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;

/**
 * @Route("comment/")
 */
class CommentController extends Controller
{
    /**
     * @Route("list/")
     * @Route("list/{sort}", name="list", defaults={"sort":"desc"})
     * @Template("AppBundle::comment.html.twig")
     */
    public function listAction(Request $request, $sort = 'desc')
    {
        $sortBy = array('asc', 'desc');
        if (!in_array($sort, $sortBy, true)) {
            $sort = 'desc';
        }
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Comment::class)->findBy(array('moderated' => true), array('createdAt' => $sort));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $comments,
            $request->query->get('page', 1),
            $this->container->getParameter('comments_per_page')
        );

        return array(
            'sort'     => $sort,
            'comments' => $pagination,
        );
    }

    /**
     * @Route("add", name="add")
     * @Template("AppBundle::formAddComment.html.twig")
     */
    public function addComment(Request $request)
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment, array(
            'action' => $this->generateUrl('add'),
        ));

        $form->handleRequest($request);

        $added = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $comment->getAvatar();
            if($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('avatar_directory'),
                    $fileName
                );
                $comment->setAvatar($fileName);
            }
            $comment->setCreatedAt(new \DateTime());
            $comment->setModerated(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $added = true;
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment, array(
                'action' => $this->generateUrl('add'),
            ));
        }
        return array(
            'added' => $added,
            'form'  => $form->createView(),
        );
    }

    /**
     * @Route("adminPanel/", name="adminPanel")
     * @Template("AppBundle::adminPanel.html.twig")
     */
    public function adminPanelAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Comment::class)->findBy(array(), array('createdAt' => 'desc'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $comments,
            $request->query->get('page', 1),
            $this->container->getParameter('comments_per_page')
        );
        return array(
            'comments' => $pagination,
        );
    }

    /**
     * @Route("delete/{id}", name="delete", requirements={"id": "^\d+$"})
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository(Comment::class)->find($id);
        $em->remove($comment);
        $em->flush();
        return $this->redirectToRoute('adminPanel');
    }

    /**
     * @Route("edit/{id}/{action}", name="edit", requirements={"id": "^\d+$"})
     */
    public function editAction($id, $action)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository(Comment::class)->find($id);
        if ($action == 'hide') {
            $comment->setModerated(false);
            $moderated = true;
        } elseif ($action == 'publish') {
            $comment->setModerated(true);
            $moderated = 0;
        }
        $em->persist($comment);
        $em->flush();
        return $this->redirectToRoute('adminPanel', array('moderated' => $moderated));
    }
}